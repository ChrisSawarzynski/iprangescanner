package pl.sawarzynski;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;

public class UrlScannerThread implements Runnable  {
    UrlGenerator urlGenerator;

    public UrlScannerThread(UrlGenerator urlGenerator) {
        this.urlGenerator = urlGenerator;
    }

    public void run() {
        for (URL url : urlGenerator) {
            try {
                if (url == null) {
                    System.out.println("Null url");
                    continue;
                }
                URLConnection connection = url.openConnection();

                if (connection == null) {
                    System.out.println(
                            MessageFormat.format("Null connection: {0}", url.toString())
                    );
                    continue;
                }

                connection.connect();
                System.out.println(
                        MessageFormat.format("Works: {0}", url.toString())
                );
            } catch (IOException e) {
                if (false)
                System.out.println(
                        MessageFormat.format("Failed: {0}", url.toString())
                );
            }
        }
    }

}
