package pl.sawarzynski;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class Main {
    public static void main(String[] args) {
        int threadCount = 50;
        UrlGenerator urlGenerator = new UrlGenerator("http://192.168.100.", 0, 255);
        ExecutorService threadExecutors = Executors.newFixedThreadPool(threadCount);

        for (int i=0; i < threadCount; i++) {
            UrlScannerThread newThread = new UrlScannerThread(urlGenerator);
            threadExecutors.submit(newThread);
        }
    }
}
