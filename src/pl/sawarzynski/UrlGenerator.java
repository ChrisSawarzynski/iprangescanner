package pl.sawarzynski;

import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

public class UrlGenerator implements Iterable<URL> {
    protected String base;
    protected int start;
    protected int current;
    protected int stop;

    public UrlGenerator(String base, int start, int stop) {
        this.base = base;
        this.start = start;
        this.current = start;
        this.stop = stop;
    }

    public Iterator<URL> iterator() {
        return new Iterator<>() {
            @Override
            public synchronized boolean hasNext() {
                return current < stop;
            }

            @Override
            public synchronized URL next() {
                String next;
                synchronized(this){
                    next = base + Integer.toString(current++);
                }

                try {
                    return new URL(next);
                } catch (MalformedURLException e) {
                    return null;
                }
            }
        };
    }
}
